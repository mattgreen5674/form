<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
//use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;

class FormTest extends TestCase 
{
    use WithoutMiddleware, DatabaseTransactions;
    /**
     * Test Form Controller savedFormData() API works.
     *
     * @return void
     */
    public function testSaveFormData()
    {
        $response = $this->post('/form/saveFormData', [
            'name' => 'Test Name',
            'email' => 'test@test.com',
            'house_name_number' => 'Test House',
            'address_line_1' => 'Test Street',
            'address_line_2' => 'Upper Testing',
            'town_city' => 'Testington',
            'county' => 'Test',
            'post_code' => 'TE5 5ET'
        ]);

        $response->assertStatus(200);
        $response->assertJson(['success' => true]);
    }
    
    /**
     * Test Form data is returned from the server.
     *
     * @return void
     */
    public function testGetFormData()
    {
        $this->post('/form/saveFormData', [
            'name' => 'Test Name',
            'email' => 'test@test.com',
            'house_name_number' => 'Test House',
            'address_line_1' => 'Test Street',
            'address_line_2' => 'Upper Testing',
            'town_city' => 'Testington',
            'county' => 'Test',
            'post_code' => 'TE5 5ET'
        ]);

        $response = $this->get('/form/getFormData');

        $response->assertStatus(200);
        $response->assertJson(['name' => 'Test Name']);
    }

}
