<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds to pre-fill the user table.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name'          => 'matt form',
                'email'         => 'matt@form.com',
                'password'      => Hash::make('form'),
                'type'          => 'form user'

            ]
        ]);
    }
}
