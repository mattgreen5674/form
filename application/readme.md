## About the Form Project

This is a simple project designed to allow a user to record some either personal or user information.  The system should validate the supplied information and save this into the project database on the back end.  Finally, all saved records should be returned, so they can be displayed in a table on the front end.

## Getting Started

- Clone the project using the supplied Bitbucket reference.
- There are two **.env.example** files contained within the project when you first open it:
    1) In the main main project folder - **'Form'**
    2) In the **'application'** folder.
    
    These need to be copied and saved as **.env** files - these files contain all of the project environment settings required to get the project up and running.
- Open a terminal window and navigate the file system to your newly saved **'Form'** project (something like this... C:\my_projects\form). Type **make build** and this will begin the project set up process and build the docker containers used to run the project and development environment.
- Before we can dive straight into project and open up the web page, we need to perform the final few configuration and project set up tasks:
    - Laravel needs a **APP_KEY** to run properly.  To generate this we need to return to oour terminal and change directory from the main project ('Form'), down into the **'Application'** - (something like this... C:\my_projects\form\application).
        -   type the following - php artisan key:generate
        
    This will create the required key and automagically add it to your .env file ready for use.
- Once done, chnage directory once mmore back up into the form folder ('cd ..' - should get you there - if not 'cd /' will take you to the top menu and you can navigate back to your project folder).  Now we want to populate our database and get it ready for saving our records.  Once in the correct folder, run **make seed** and this will add the following:
    - A user table and user.  You'll need the below to login.
        - user email = **matt@form.com**
        - password = **form** 
    - A password reset table - default table in laravel.
    - Form table.
    - User Table Seeder - to add the above user to the user table.

    Sometimes you will get a 'reflection error' when running this command.  To resolve this navigate back into the 'application' folder and run the following:
    - composer dump-autoload
    
    This will reset the saved details and enable to navigate back up to the 'form' folder and rerun the **make seed command**

- Final potential hoop to jump through.  

    Fire up your browser of choice and go to:
    - http:\\localhost:8000 
    
    If everything has worked then you should see the welcome page and be able to login to access the form.

    If there is a problem, this is usually caused by not all the node js packages having installed properly - although this should have happened as part of the build process.

    Once more, change directory to the 'application' folder and run the following:
    - npm install
    - change directory back up to the 'form' folder and:
        - make stop
        - make start

    Fingers crossed, this should have resolved any problems and you will be able to get stuck in.