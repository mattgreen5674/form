import React, { Component } from 'react';

class Input extends Component {
    constructor(props){
        super(props);

        this.changeValue = this.changeValue.bind(this);
        
    }

    changeValue(event){
        this.props.setValue(event.target.name, event.target.value)
    }
    

    render() {
        return (
            <div>
                <label htmlFor="name">{this.props.title}</label>
                <div className="w-100">
                    <div className={this.props.class}>
                        <input type="text" className="form-control" name={this.props.name} placeholder={this.props.placeHolder} value={this.props.getValue} onChange={this.changeValue}/>
                    </div>
                    <div className={!this.props.inValid ? this.props.errorClass : 'd-none'}>
                        {this.props.errorMessage}
                    </div>
                </div>
            </div>
        );
    }
}

export default Input;