import React, { Component } from 'react';
import ReactDOM from 'react-dom';

export default class Form extends Component {
    constructor(props){
        super(props);

        this.loadTable = this.loadTable.bind(this);
    }
    
    loadTable(){
        if (this.props.formRecords.length > 0){
            let tableRows = this.props.formRecords.map((form) => {
                return <tr key={form.id}>
                        <td>{form.name}</td>
                        <td>{form.email}</td>
                        <td>{form.house_name_number}</td>
                        <td>{form.address_line_1}</td>
                        <td>{form.address_line_2}</td>
                        <td>{form.town_city}</td>
                        <td>{form.county}</td>
                        <td>{form.post_code}</td>
                    </tr>;
            });
            return tableRows;
        }else{
            return <tr><td colSpan="8">There are no records saved, as yet</td></tr>
        }
    }

    render() {
        return (
            <div className="row justify-content-center mt-4">
                <div className="col-md-8">
                    <div className="card">
                        <div className="card-header"><b>Form Data Table</b></div>

                        <div className="card-body">
                            <table className="table table-striped table-bordered table-sm">
                                <thead className="thead-dark">
                                    <tr>
                                        <th scope="col">Name</th>
                                        <th scope="col">Email</th>
                                        <th scope="col">House No.</th>
                                        <th scope="col">Address 1</th>
                                        <th scope="col">Address 2</th>
                                        <th scope="col">Town</th>
                                        <th scope="col">County</th>
                                        <th scope="col">Post Code</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.loadTable()}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}


