
import React, { Component } from 'react';
import Input from './input.js'
import Validation from '../helpers/Validation.js'

export default class Form extends Component {
    constructor(props){
        super(props)

            this.state = {
                name : "",
                email : "",
                house : "",
                ad1 : "",
                ad2 : "",
                town : "",
                county : "",
                pc : "",

                validName : true,
                validEmail : true,
                validHouse : true,
                validAd1 : true,
                validAd2 : true,
                validTown : true,
                validCounty : true,
                validPc : true
            }
        
        this.setValue = this.setValue.bind(this);
        this.validateForm = this.validateForm.bind(this);
        //this.submitData = this.submitData.bind(this);
    }

    setValue(target, value){
        this.setState({
            [target] : value
        });
    }

    validateForm(){
        console.log(this.state.pc);
        const validator = new Validation();
        const nameOk = validator.validText(this.state.name);
        const emailOk = validator.validEmail(this.state.email);
        const houseOk = validator.validText(this.state.house);
        const ad1Ok = validator.validText(this.state.ad1);
        const ad2Ok = validator.validText(this.state.ad2);
        const townOk = validator.validText(this.state.town);
        const countyOk = validator.validText(this.state.county);
        const pcOk = validator.validPostCode(this.state.pc);

        //ad2 needs an override as it could very well be an empty input - not everyone has a complicated or long address.
        //Not ideal
        const ad2OkOverride = (ad2Ok)? ad2Ok: true;

        if(nameOk && emailOk && houseOk && ad1Ok && ad2OkOverride && townOk && countyOk && pcOk){
            this.submitData()
        }else(
            this.setState({
                validName : nameOk,
                validEmail : emailOk,
                validHouse : houseOk,
                validAd1 : ad1Ok,
                validAd2 : ad2Ok,
                validTown : townOk,
                validCounty : countyOk,
                validPc : pcOk
            })
        )
    }

    submitData(){
        
        let data = {
            'name'              : this.state.name,
            'email'             : this.state.email,
            'house_name_number' : this.state.house,
            'address_line_1'    : this.state.ad1,
            'address_line_2'    : (this.state.ad2)? this.state.ad2: '',
            'town_city'         : this.state.town,
            'county'            : this.state.county,
            'post_code'         : this.state.pc
        };

        this.props.submitData(data);
    }

    render() {
        return (
        <div className="row justify-content-center">
            <div className="col-md-8">
                <div className="card">
                    <div className="card-header">
                        <b>Form Submission Template</b>
                    </div>

                    <div className="card-body">
                        <h5><b>User Details</b></h5>
                        
                        <Input name='name' title='Name' getValue={this.state.name} class='input-group mb-2 w-50' errorClass='alert alert-danger w-75 pt-1 pb-1 mb-2' setValue={this.setValue} inValid={this.state.validName} errorMessage={'You must supply a valid name - for example: John Doe.'} placeHolder={'example: John Doe'} required />
                        <Input name='email' title='Email' getValue={this.state.email} class='input-group mb-2 w-50' errorClass='alert alert-danger w-75 pt-1 pb-1 mb-2' setValue={this.setValue} inValid={this.state.validEmail} errorMessage={'Please enter a valid email - for example: john@doe.com'} placeHolder={'example: john@doe.com'} required />
                        <hr/>
                        <h5><b>Address Details</b></h5>
                        <Input name='house' title='House Name or Number' getValue={this.state.house} class='input-group mb-2 w-50' errorClass='alert alert-danger w-100 pt-1 pb-1 mb-2' setValue={this.setValue} inValid={this.state.validHouse} errorMessage={'Please enter a valid house number or name - for example: 42 or The Heights'} placeHolder={'example: 42 or The Heights'} required />
                        <Input name='ad1' title='Address Line 1' getValue={this.state.ad1} class='input-group mb-2 w-75' errorClass='alert alert-danger w-75 pt-1 pb-1 mb-2' setValue={this.setValue} inValid={this.state.validAd1} errorMessage={'Please enter a valid address - for example: The High Street'} placeHolder={'example: The High Street'} required />
                        <Input name='ad2' title='Address Line 2' getValue={this.state.ad2} class='input-group mb-2 w-75' errorClass='alert alert-danger w-75 pt-1 pb-1 mb-2' setValue={this.setValue} inValid={this.state.validAd2} errorMessage={'Please enter a valid address - for example: The High Street'} placeHolder={'example: The High Street'} required />
                        <Input name='town' title='Town or City' getValue={this.state.town} class='input-group mb-2 w-50' errorClass='alert alert-danger w-75 pt-1 pb-1 mb-2' setValue={this.setValue} inValid={this.state.validTown} errorMessage={'Please enter a valid town or city - for example: Bournemouth or London'} placeHolder={'example: Bournemouth or London'} required />
                        <Input name='county' title='County' getValue={this.state.county} class='input-group mb-2 w-50' errorClass='alert alert-danger w-75 pt-1 pb-1 mb-2' setValue={this.setValue} inValid={this.state.validCounty} errorMessage={'Please enter a valid town or city - for example: Dorset or Greter London'} placeHolder={'example: Dorset or Greter London'} required />
                        <Input name='pc' title='Post Code' getValue={this.state.pc} class='input-group mb-2 w-25' errorClass='alert alert-danger w-75 pt-1 pb-1 mb-2' setValue={this.setValue} inValid={this.state.validPc} errorMessage={'Please enter a valid post code - for example: BH1 1HB'} placeHolder={'example: BH1 1HB'} required />
                        
                        <button className="btn btn-dark" onClick={this.validateForm}>Submit</button>
                    </div>
                </div>
            </div>
        </div>
        
        );
    }
}


