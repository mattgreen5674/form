
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes React and other helpers. It's a great starting point while
 * building robust, powerful web applications using React + Laravel.
 */

require('./bootstrap');

import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Form from './components/Form'
import Table from './components/Table'

class App extends Component {
    constructor(props){
        super(props);

        this.state = {
            formRecords : [] 
        }

        this.submitData = this.submitData.bind(this);
    }

    componentDidMount(){
        this.getData();
    }

    async getData(){
        const url = "http://localhost:8000/form/getFormData";
        try{
            const response = await axios.get(url);
            if(response['data'].length != 0){
                this.setState({
                    formRecords : response['data']
                });
            }
        }catch(error){

        };
    }

    async submitData(data){
        const url = "http://localhost:8000/form/saveFormData";
        const token = document.querySelector('meta[name="csrf-token"]').content;

        try {
            const response = await axios.post(
                url, 
                data,
                {headers: {'X-CSRF-TOKEN' : token, 'Content-type': 'application/json; charset=utf-8'}}
            );
            this.getData();
        } catch (error) {
            
        }
    }

    render() {
        return (
            <div>
                <Form submitData={this.submitData} />
                <Table formRecords={this.state.formRecords} />
            </div>
        );
    }
}

if (document.getElementById('root')) {
    ReactDOM.render(<App />, document.getElementById('root'));
}