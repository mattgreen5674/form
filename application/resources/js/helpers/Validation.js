class Validation {
  
    isNotEmptyString(value){
        return (value.length > 0 && value != null && value != '')? true : false;
    }

    isEmail(value){
        const emailPattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return (value.match(emailPattern))? true : false;
    }

    isPhone(value){
        const phonePattern1 = /^\d{11}$/;
        const phonePattern2 = /^\(?([0-9]{5})\)?[-. ]?([0-9]{6})$/
        return (value.match(phonePattern1) || value.match(phonePattern2))? true : false;
    }

    isPostCode(value){
        const postCodePattern = /([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|(([A-Za-z][0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9][A-Za-z]?))))\s?[0-9][A-Za-z]{2})/;
        return (value.match(postCodePattern))? true : false;
    }

    validText(value){
        return (this.isNotEmptyString(value))? true : false;
    }

    validEmail(value){
        return (this.isNotEmptyString(value) && this.isEmail(value))? true : false;
    }

    validPhone(value){
        return (this.isNotEmptyString(value) && this.isPhone(value))? true : false;
    }

    validPostCode(value){
        return (this.isNotEmptyString(value) && this.isPostCode(value))? true : false;
    }
  
}

export default Validation;



