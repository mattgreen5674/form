<?php

namespace App\Http\Controllers;

use App\Models\Form;
use App\Http\Requests\StoreForm;
use Illuminate\Http\Request;

class FormController extends Controller
{
    protected $form;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the form view.
     *
     * @return form view
     */
    public function index()
    {
        return view('form');
    }


    /** API End Point to enable us to retrieve stored data from the form 
     *  database table.
     */ 
    public function getFormData(){
        
        $data = Form::all();

        return json_encode($data);
    }

    /** API End Point to enable us to save data supplied by a form 
     *  to the database.
     *  
     *  @params Request $request - this data set contains the following:
     *  @return array success = true / false 
     */
    public function saveFormData(StoreForm $request){
        
        $savedOk = Form::create($request->all());
        
        return json_encode(['success' => $savedOk]);
    }
}
