<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'bail|required|string',
            'email' => 'bail|required|unique:forms|email',
            'house_name_number' => 'bail|required|string',
            'address_line_1' => 'bail|required|string', 
            'address_line_2' => 'bail|nullable|string', 
            'town_city' => 'bail|required|string', 
            'county' => 'bail|required|string',
            'post_code' => 'bail|required|string',
        ];
    }
}
