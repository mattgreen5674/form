<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
    //Form model class to handle CRUD interactions with the DB.
    //This is empty as no custom actions have been required for this project.
    protected $fillable = ['name', 'email', 'house_name_number', 'address_line_1', 'address_line_2', 'town_city', 'county','post_code'];
}